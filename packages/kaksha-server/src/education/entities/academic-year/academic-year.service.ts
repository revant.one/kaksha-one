import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { AcademicYear } from './academic-year.entity';

@Injectable()
export class AcademicYearService {
  constructor(
    @InjectRepository(AcademicYear)
    private readonly repo: MongoRepository<AcademicYear>,
  ) {}

  async save(params) {
    return await this.repo.save(params);
  }

  async find(params?): Promise<AcademicYear[]> {
    return await this.repo.find(params);
  }

  async findOne(params) {
    return await this.repo.findOne(params);
  }

  async updateOne(query, params) {
    return await this.repo.updateOne(query, params);
  }

  async updateMany(query, params) {
    return await this.repo.updateMany(query, params);
  }

  async count() {
    return await this.repo.count();
  }

  async paginate(
    skip: number,
    take: number,
    search: string,
    order: any,
    type: string,
  ) {
    skip = Number(skip);
    take = Number(take);
    const nameExp = new RegExp(search, 'i');
    const keys = this.repo.metadata.columns;
    const $or = keys.map(column => {
      const filter = {};
      filter[column.propertyName] = nameExp;
      return filter;
    });

    const where: { $or?: any; type?: string } = { $or };
    if (type) where.type = type;

    const docs = await this.repo.find({
      skip,
      take,
      order,
      where,
    });

    const length = await this.repo.count({ ...where });
    return { docs, length, offset: skip ? skip : undefined };
  }

  async deleteMany(params) {
    return await this.repo.deleteMany(params);
  }
}
