import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { Standard } from './standard.entity';

@Injectable()
export class StandardService {
  constructor(
    @InjectRepository(Standard)
    private readonly repo: MongoRepository<Standard>,
  ) {}

  async save(params) {
    return await this.repo.save(params);
  }

  async find(params?): Promise<Standard[]> {
    return await this.repo.find(params);
  }

  async findOne(params) {
    return await this.repo.findOne(params);
  }

  async updateOne(query, params) {
    return await this.repo.updateOne(query, params);
  }

  async updateMany(query, params) {
    return await this.repo.updateMany(query, params);
  }

  async count() {
    return await this.repo.count();
  }

  async paginate(
    skip: number,
    take: number,
    search: string,
    order: any,
    type: string,
  ) {
    skip = Number(skip);
    take = Number(take);
    const nameExp = new RegExp(search, 'i');
    const keys = this.repo.metadata.columns;
    const $or = keys.map(column => {
      const filter = {};
      filter[column.propertyName] = nameExp;
      return filter;
    });

    const where: { $or?: any; type?: string } = { $or };
    if (type) where.type = type;

    const docs = await this.repo.find({
      skip,
      take,
      order,
      where,
    });

    const length = await this.repo.count({ ...where });
    return { docs, length, offset: skip ? skip : undefined };
  }

  async deleteMany(params) {
    return await this.repo.deleteMany(params);
  }
}
