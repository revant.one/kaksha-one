import { Student } from './student/student.entity';
import { Teacher } from './teacher/teacher.entity';
import { Organization } from './organization/organization.entity';
import { AcademicYear } from './academic-year/academic-year.entity';
import { Standard } from './standard/standard.entity';
import { Subject } from './subject/subject.entity';
import { Topic } from './topic/topic.entity';
import { Quiz } from './quiz/quiz.entity';
import { Attendance } from './attendance/attendance.entity';

import { TeacherService } from './teacher/teacher.service';
import { OrganizationService } from './organization/organization.service';
import { AcademicYearService } from './academic-year/academic-year.service';
import { StandardService } from './standard/standard.service';
import { SubjectService } from './subject/subject.service';
import { TopicService } from './topic/topic.service';
import { QuizService } from './quiz/quiz.service';
import { AttendanceService } from './attendance/attendance.service';

export const EducationEntityServices = [
  TeacherService,
  OrganizationService,
  AcademicYearService,
  StandardService,
  SubjectService,
  TopicService,
  QuizService,
  AttendanceService,
];

export const EducationEntities = [
  Student,
  Teacher,
  Organization,
  AcademicYear,
  Standard,
  Subject,
  Topic,
  Quiz,
  Attendance,
];
