import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';
import { SystemSettingsModule } from './system-settings/system-settings.module';
import { connectTypeORM } from './constants/typeorm.connection';
import { ConfigService } from './config/config.service';
import { CommonModule } from './common/common.module';
import { EducationModule } from './education/education.module';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: connectTypeORM,
      inject: [ConfigService],
    }),
    ConfigModule,
    CommonModule,
    AuthModule,
    SystemSettingsModule,
    EducationModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
